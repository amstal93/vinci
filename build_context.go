// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package vinci

import (
	"context"
	"fmt"

	"github.com/hashicorp/go-hclog"
	"gitlab.com/thriqon/vinci/pkg/cache"
	"gitlab.com/thriqon/vinci/pkg/client"
	"golang.org/x/sync/semaphore"
)

type buildContextKey int

const (
	bcClient buildContextKey = iota
	bcCache
	bcBuildSem
	bcBaseDir
)

func buildContextFromConfig(ctx context.Context, bc BuildConfig) context.Context {
	L(ctx).Trace("build-context", "workers", bc.ParallelWorkers)

	if bc.ParallelWorkers <= 0 {
		panic("parallel workers has to be greater than 0")
	}

	return context.WithValue(context.WithValue(context.WithValue(context.WithValue(ctx, bcCache, bc.Cache), bcClient, bc.Client), bcBuildSem, semaphore.NewWeighted(int64(bc.ParallelWorkers))), bcBaseDir, bc.BaseDir)
}

// L extracts the hclog.Logger from the given context.  It wraps
// hclog.FromContext to provide a consistent interface.
func L(ctx context.Context) hclog.Logger {
	return hclog.FromContext(ctx)
}

// Cache extracts the Cache instance from the given context. If there is no
// Cache stored in the context, it panics.
func Cache(ctx context.Context) cache.Cache {
	v := ctx.Value(bcCache).(cache.Cache)
	if v == nil {
		panic("no cache in context")
	}

	return v
}

// Client extracts the Client pointer from the given context. If there is no
// Client stored in the context, it panics.
func Client(ctx context.Context) *client.Client {
	v := ctx.Value(bcClient).(*client.Client)
	if v == nil {
		panic("no client in context")
	}

	return v
}

func buildSemaphore(ctx context.Context) *semaphore.Weighted {
	v := ctx.Value(bcBuildSem).(*semaphore.Weighted)
	if v == nil {
		panic("no semaphore in context")
	}

	return v
}

func logWith(ctx context.Context, v ...interface{}) context.Context {
	return hclog.WithContext(ctx, hclog.FromContext(ctx), v...)
}

// BaseDir extracts the directory serving as base for all relative path
// calculations from the given context. It panics if there is no base dir set.
func BaseDir(ctx context.Context) string {
	return ctx.Value(bcBaseDir).(string)
}

// AcquireToken blocks until a slot from the contexts semaphore is reserved.
// The semaphore is initialized with the ParallelWorkers setting in the
// BuildConfig. The given name is used to enrich the logs.
func AcquireToken(ctx context.Context, name string) error {
	err := buildSemaphore(ctx).Acquire(ctx, 1)
	if err != nil {
		L(ctx).Warn("unable to acquire token", "err", err, "name", name)

		return fmt.Errorf("acquire token: %w", err)
	}

	L(ctx).Info("acquired token", "name", name)

	return nil
}

// ReleaseToken releases a slot reserved by AcquireToken. It is an error to
// release a token that was never acquired (this is not enforced by this
// method).
func ReleaseToken(ctx context.Context) {
	buildSemaphore(ctx).Release(1)
}
