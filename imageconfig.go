// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package vinci

import (
	"context"
	"fmt"

	digest "github.com/opencontainers/go-digest"
	imagev1 "github.com/opencontainers/image-spec/specs-go/v1"
)

type image struct {
	imagev1.Image
}

func (ic *image) SetLayers(ctx context.Context, descs []imagev1.Descriptor) error {
	ic.RootFS.Type = "layer"

	ic.RootFS.DiffIDs = make([]digest.Digest, len(descs))
	for i := range descs {
		layerIDBuf, err := Cache(ctx).ReadMeta(ctx, descs[i].Digest, LayerID)
		if err != nil {
			return fmt.Errorf("unable to get diff id for %s: %w", descs[i].Digest, err)
		}

		dgst, err := digest.Parse(string(layerIDBuf))
		if err != nil {
			return fmt.Errorf("unable to parse diff id %s: %w", string(layerIDBuf), err)
		}

		ic.RootFS.DiffIDs[i] = dgst
	}

	return nil
}
