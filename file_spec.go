// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package vinci

import (
	"archive/tar"
	"context"

	// Digester wants to have this imported anonymously.
	_ "crypto/sha256"
	"fmt"
	"io"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	hclog "github.com/hashicorp/go-hclog"
	imagev1 "github.com/opencontainers/image-spec/specs-go/v1"
	v1config "gitlab.com/thriqon/vinci/pkg/config/v1"
)

type FileLayerSpec struct {
	Path     string
	Mode     int
	Contents string
	Source   string
}

var _ CombinableSpec = FileLayerSpec{}

func NewFileLayerSpecV1(flsc v1config.FileLayerSpec) (FileLayerSpec, error) {
	if flsc.Contents != "" && flsc.Source != "" {
		return FileLayerSpec{}, fmt.Errorf("contents and source may not both be set: %w", ErrUninterpretableValue)
	}

	return FileLayerSpec{
		Path:     flsc.Path,
		Mode:     flsc.Mode,
		Contents: flsc.Contents,
		Source:   flsc.Source,
	}, nil
}

func (fls FileLayerSpec) EnsureInCache(ctx context.Context) ([]imagev1.Descriptor, error) {
	L(ctx).Debug("ensure file in cache", "path", fls.Path)

	if err := AcquireToken(ctx, "file-spec/"+fls.Path); err != nil {
		return nil, err
	}
	defer ReleaseToken(ctx)

	return ensureMultisInCache(ctx, []CombinableSpec{fls})
}

func (fls FileLayerSpec) provideBytesAsEntries(logger hclog.Logger, w *tar.Writer) error {
	p := strings.TrimPrefix(fls.Path, "/")

	fileLen := int64(len(fls.Contents))

	th := tar.Header{
		Name:       p,
		Size:       fileLen,
		Mode:       int64(fls.Mode),
		Uid:        0,
		Gid:        0,
		ModTime:    time.Unix(0, 0).UTC(),
		AccessTime: time.Unix(0, 0).UTC(),
		ChangeTime: time.Unix(0, 0).UTC(),
	}

	if err := w.WriteHeader(&th); err != nil {
		return fmt.Errorf("unable to write header: %w", err)
	}

	n, err := w.Write([]byte(fls.Contents))
	if err != nil {
		return fmt.Errorf("unable to write contents: %w", err)
	}

	logger.Trace("used directly", "n", n)

	return nil
}

func copyFileInto(w io.Writer, fname string) (int64, error) {
	f, err := os.Open(fname)
	if err != nil {
		return 0, fmt.Errorf("copy into %s: %w", fname, err)
	}

	defer f.Close()

	return io.Copy(w, f)
}

func (fls FileLayerSpec) providePathAsEntries(ctx context.Context, logger hclog.Logger, w *tar.Writer) error {
	logger.Trace("reading from path", "souce", fls.Source)

	err := filepath.Walk(fls.Source, func(cpath string, info os.FileInfo, err error) error {
		if err != nil {
			return fmt.Errorf("walking %s: %w", cpath, err)
		}

		if err := ctx.Err(); err != nil {
			return fmt.Errorf("walking %s: %w", cpath, err)
		}

		relativizedPath, err := filepath.Rel(fls.Source, cpath)
		if err != nil {
			return fmt.Errorf("walk %s for %s: %w", fls.Source, cpath, err)
		}

		targetPath := strings.TrimPrefix(path.Join(fls.Path, filepath.ToSlash(relativizedPath)), "/")
		logger.Trace("copy as entry", "target-path", targetPath, "path", cpath)

		th := tar.Header{
			Name:       targetPath,
			Size:       info.Size(),
			Mode:       int64(info.Mode().Perm()),
			Uid:        0,
			Gid:        0,
			ModTime:    time.Unix(0, 0).UTC(),
			AccessTime: time.Unix(0, 0).UTC(),
			ChangeTime: time.Unix(0, 0).UTC(),
		}

		if info.IsDir() {
			th.Typeflag = tar.TypeDir
			th.Size = 0
			th.Name += "/"
		}

		if err := w.WriteHeader(&th); err != nil {
			return fmt.Errorf("unable to write header: %w", err)
		}

		if info.IsDir() {
			return nil
		}

		n, err := copyFileInto(w, cpath)
		if err != nil {
			return fmt.Errorf("copy %s: %w", cpath, err)
		}

		logger.Trace("copy as entry - done", "target-path", targetPath, "path", cpath, "n", n)

		return nil
	})
	if err != nil {
		return fmt.Errorf("walk %s: %w", fls.Source, err)
	}

	return nil
}

func (fls FileLayerSpec) ProvideAsTarEntries(ctx context.Context, w *tar.Writer) error {
	logger := L(ctx).With("path", fls.Path)
	logger.Debug("provide file as tar entries")

	if fls.Contents != "" {
		logger.Trace("using content from yaml")

		return fls.provideBytesAsEntries(logger, w)
	}

	return fls.providePathAsEntries(ctx, logger, w)
}
