# Vinci

<img src="assets/vinci.png" align="right" width="150">

Declarative and Reproducible Container Images

* Declarative: An image built with `vinci` is not 'run', it's
  assembled using the layer specs.
* Reproducible: All possible steps are designed to _always_
  return exactly the same layers, given the same inputs
  (for the mathematically inclined: this is a pure function).

## Examples

The following examples shows how to create a very simple image,
printing 'Hello, World' when executed as a container. When assembled
with `vinci`, it will have the digest
`sha256:95ee59c6c44c8de465a6e78661edd90d7fe7e52b4db15e8539a528ce5a1f7551`.

```yaml
spec-version: v1
image: registry.gitlab.com/thriqon/vinci/simplest
config:
  entrypoint:
    - /cat
    - /message
layers:
  - file:
      path: /cat
      source: ./busybox
  - file:
      path: /message
      mode: 0444
      contents: |
        Hello, World!
```

## Quickstart

Fetch `vinci` from the latest build:

```bash
wget 'https://gitlab.com/thriqon/vinci/-/jobs/artifacts/master/raw/vinci?job=build' && chmod 0755 vinci
```

## License

Vinci is licensed under the Apache License 2.0. See the file
called LICENSE for more information.
