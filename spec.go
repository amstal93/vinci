// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package vinci

import (
	"archive/tar"
	"context"
	"errors"
	"fmt"

	imagev1 "github.com/opencontainers/image-spec/specs-go/v1"
	configv1 "gitlab.com/thriqon/vinci/pkg/config/v1"
)

type Spec interface {
	EnsureInCache(context.Context) ([]imagev1.Descriptor, error)
}

type CombinableSpec interface {
	Spec

	ProvideAsTarEntries(context.Context, *tar.Writer) error
}

var ErrUninterpretableValue = errors.New("unable to interpret value")

func AsSpec(c configv1.LayerSpec) (Spec, error) {
	switch {
	case c.Combine != nil:
		return NewCombinedSpec(c.Combine)

	case c.File != nil:
		return NewFileLayerSpecV1(*c.File)

	case c.ImportOCI != nil:
		return NewImportOCISpecV1(*c.ImportOCI)

	case c.Import != nil:
		return NewImportSpecV1(*c.Import)
	}

	return nil, fmt.Errorf("unable to convert %v to spec: %w", c, ErrUninterpretableValue)
}
