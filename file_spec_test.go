// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package vinci_test

import (
	"bytes"
	"context"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/thriqon/vinci"
	"gitlab.com/thriqon/vinci/pkg/cache"
	v1config "gitlab.com/thriqon/vinci/pkg/config/v1"
)

func TestFileLayerSpec_BothSourceAndContents(t *testing.T) {
	t.Parallel()

	_, err := vinci.NewFileLayerSpecV1(v1config.FileLayerSpec{
		Contents: "contents",
		Source:   "/tmp/test",
	})

	assert.Error(t, err)
}

func fileTestContext() context.Context {
	return vinci.BuildContextFromConfig(context.Background(), vinci.BuildConfig{
		Cache:           cache.NewMemCache(),
		Client:          nil,
		ParallelWorkers: 1,
	})
}

func TestFileLayerSpec(t *testing.T) {
	t.Parallel()

	// given
	ctx := fileTestContext()

	fls := vinci.FileLayerSpec{
		Path:     "/usr/local/bin/hello",
		Mode:     0o753,
		Contents: "#!/bin/bash\necho hello\n",
	}

	// when
	descriptors, err := fls.EnsureInCache(ctx)
	require.NoError(t, err)
	require.Len(t, descriptors, 1)

	// then
	cmd := exec.Command("tar", "vzt")
	cmd.Env = append(os.Environ(), "TZ=UTC")

	rc, size, err := vinci.Cache(ctx).Open(ctx, descriptors[0].Digest)
	require.NoError(t, err)

	defer rc.Close()

	require.NotZero(t, size)

	var buf bytes.Buffer
	cmd.Stderr = &buf
	cmd.Stdout = &buf

	stdin, err := cmd.StdinPipe()
	require.NoError(t, err)

	require.NoError(t, cmd.Start())

	_, err = io.Copy(stdin, rc)
	require.NoError(t, err)

	require.NoError(t, stdin.Close())
	execErr := cmd.Wait()

	t.Logf("output:\n%s", buf.String())
	assert.Equal(t, buf.String(), "-rwxr-x-wx 0/0              23 1970-01-01 00:00 usr/local/bin/hello\n")

	assert.NoError(t, execErr)

	// Reproduction:
	// $ cat usr/local/bin/hello
	// #!/bin/bash
	// echo hello
	// $ sha256sum usr/local/bin/hello
	// f590776b449af73e55cb368f45ce28400a19d0c68cdb34485fdfc0602b6c2437  usr/local/bin/hello
	// $ tar cf test.tar -H ustar --record-size 1024 --mtime 1970-01-01T00:00:00Z --owner 0 --group 0 --numeric-owner usr/local/bin/hello
	// $ sha256sum test.tar
	// 0192d1634d5d24e96bac79e5e0eb4c587f51cc86c1e030eefa422d27dbad4ddd  test.tar

	layerID, err := vinci.Cache(ctx).ReadMeta(ctx, descriptors[0].Digest, vinci.LayerID)
	assert.NoError(t, err)
	assert.Equal(t, string(layerID), "sha256:0192d1634d5d24e96bac79e5e0eb4c587f51cc86c1e030eefa422d27dbad4ddd")
}

func TestFileLayerSpec_Directory(t *testing.T) {
	t.Parallel()

	// given
	ctx := fileTestContext()

	tmp := t.TempDir()

	require.NoError(t, os.Mkdir(filepath.Join(tmp, "src"), 0o755))
	require.NoError(t, ioutil.WriteFile(filepath.Join(tmp, "src", "a.txt"), []byte("a"), 0o400))
	require.NoError(t, ioutil.WriteFile(filepath.Join(tmp, "src", "b.txt"), []byte("b"), 0o600))
	require.NoError(t, os.MkdirAll(filepath.Join(tmp, "src", "d1", "d2", "d3"), 0o755))
	require.NoError(t, ioutil.WriteFile(filepath.Join(tmp, "src", "d1", "d2", "x.txt"), []byte("x"), 0o700)) //nolint:gosec // Need this for tests.

	fls := vinci.FileLayerSpec{
		Path:   "/in",
		Source: filepath.Join(tmp, "src"),
	}

	// when
	descriptors, err := fls.EnsureInCache(ctx)
	require.NoError(t, err)
	require.Len(t, descriptors, 1)

	// then
	layerID, err := vinci.Cache(ctx).ReadMeta(ctx, descriptors[0].Digest, vinci.LayerID)
	assert.NoError(t, err)
	assert.Equal(t, "sha256:27390ee33ee7b9de9f65949aefe3e01e1dee550e9ae8d12e9c437c4f3c3c05a6", string(layerID))

	// Reproduction:
	// $ mkdir -p in/d1/d2/d3
	// $ echo -n a > in/a.txt
	// $ echo -n b > in/b.txt
	// $ echo -n x > in/d1/d2/x.txt
	// chmod 0400 in/a.txt
	// chmod 0600 in/b.txt
	// chmod 0700 in/d1/d2/x.txt

	// $ sha256sum $(find in -type f)
	// 2d711642b726b04401627ca9fbac32f5c8530fb1903cc4db02258717921a4881  in/d1/d2/x.txt
	// 3e23e8160039594a33894f6564e1b1348bbd7a0088d42c4acb73eeaed59c009d  in/b.txt
	// ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb  in/a.txt

	// $ find in -type d
	// in
	// in/d1
	// in/d1/d2
	// in/d1/d2/d3

	// tar cf test.tar -H ustar --record-size 1024 --mtime 1970-01-01T00:00:00Z --owner 0 --group 0 --numeric-owner --sort=name in

	// $ sha256sum test.tar
	// 27390ee33ee7b9de9f65949aefe3e01e1dee550e9ae8d12e9c437c4f3c3c05a6 test.tar
}
