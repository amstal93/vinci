#!/bin/bash
set -euo pipefail

D=$(dirname "$0")

mkdir -p "$D/bin/"

_ensure() {
	local name=$1
	local hash=$2

	fname="$D/bin/$name"

	echo "> ensure $fname has hash $hash"
	[ -f "$fname" ] && sha256sum -c - <<<"$hash $fname"
}


_ensure "golangci-lint" "036f25de15e55190d495ff17ba1bec77e12f39e7911c98f609b21e279102d544" || {
	wget https://github.com/golangci/golangci-lint/releases/download/v1.36.0/golangci-lint-1.36.0-linux-amd64.tar.gz -O - | tar zx -C "$D/bin" --strip-components=1 golangci-lint-1.36.0-linux-amd64/golangci-lint

	_ensure "golangci-lint" "036f25de15e55190d495ff17ba1bec77e12f39e7911c98f609b21e279102d544"
}

_ensure "gotestsum" "88ba3cebdd5e7167fda47729611104a7c1ff5dd830f5314729659e16056a0a69" || {
	wget https://github.com/gotestyourself/gotestsum/releases/download/v1.6.1/gotestsum_1.6.1_linux_amd64.tar.gz -O - | tar zx -C "$D/bin" gotestsum

	_ensure "gotestsum" "88ba3cebdd5e7167fda47729611104a7c1ff5dd830f5314729659e16056a0a69"
}

_ensure "gofumpt" "1f97b215686198ae0a020822f7f977a7ba12dda6a9eea84bbe5ee706531b4e8a" || {
	wget https://github.com/mvdan/gofumpt/releases/download/v0.1.0/gofumpt_v0.1.0_linux_amd64 -O "$D/bin/gofumpt"

	_ensure "gofumpt" "1f97b215686198ae0a020822f7f977a7ba12dda6a9eea84bbe5ee706531b4e8a"
}

[ -x "$D/bin/gofumpt" ] || chmod +x "$D/bin/gofumpt"
