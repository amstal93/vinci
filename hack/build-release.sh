#!/bin/bash

cd "$(dirname "$0")/../"

CGO_ENABLED=0 go build -ldflags "-s -X gitlab.com/thriqon/vinci/pkg/version.Version=$(git describe --always --dirty)" ./cmd/vinci
