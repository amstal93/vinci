// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package vinci

import (
	"context"
	"fmt"

	digest "github.com/opencontainers/go-digest"
	imagev1 "github.com/opencontainers/image-spec/specs-go/v1"
	"gitlab.com/thriqon/vinci/pkg/cache"
	"gitlab.com/thriqon/vinci/pkg/client"
	configv1 "gitlab.com/thriqon/vinci/pkg/config/v1"
)

func storeConfigInCache(ctx context.Context, config imagev1.ImageConfig, layerDescriptors []imagev1.Descriptor) (imagev1.Descriptor, error) {
	data := image{}
	data.Config = config
	if err := data.SetLayers(ctx, layerDescriptors); err != nil {
		return imagev1.Descriptor{}, fmt.Errorf("unable to set layers in descriptor: %w", err)
	}

	dgst, size, err := cacheJSONObject(ctx, data)
	if err != nil {
		return imagev1.Descriptor{}, fmt.Errorf("unable to store config: %w", err)
	}

	return imagev1.Descriptor{
		MediaType: imagev1.MediaTypeImageConfig,
		Digest:    dgst,
		Size:      size,
	}, nil
}

func storeManifestInCache(ctx context.Context, manifest interface{}) (digest.Digest, error) {
	dgst, _, err := cacheJSONObject(ctx, manifest)
	if err != nil {
		return digest.Digest(""), fmt.Errorf("unable to store config: %w", err)
	}

	return dgst, nil
}

type BuildConfig struct {
	Client *client.Client
	Cache  cache.Cache

	BaseDir string

	ParallelWorkers int

	TagPrefix string
}

type manifest struct {
	SchemaVersion int    `json:"schemaVersion"`
	MediaType     string `json:"mediaType"`

	Config imagev1.Descriptor `json:"config"`

	Layers []imagev1.Descriptor `json:"layers"`
}

// DefaultParallelWorkers is the number of jobs to execute in parallel if no
// override in the BuildConfig has been set.
const DefaultParallelWorkers = 2

func (bc BuildConfig) BuildV1(ctx context.Context, conf configv1.SingleImageConfigFile) (client.ManifestRef, error) {
	if bc.ParallelWorkers == 0 {
		bc.ParallelWorkers = DefaultParallelWorkers
	}

	ctx = buildContextFromConfig(ctx, bc)

	in, err := client.ParseImageName(conf.ImageName)
	if err != nil {
		return client.ManifestRef{}, fmt.Errorf("unable to parse image name %s: %w", conf.ImageName, err)
	}

	if bc.Client == nil {
		panic("BuildConfig.Client may not be nil")
	}

	// generate layers and store in cache
	layerDescriptors, err := bc.buildV1Layers(ctx, conf.LayerSpecs, in)
	if err != nil {
		return client.ManifestRef{}, fmt.Errorf("unable to build layers: %w", err)
	}

	configDesc, err := storeConfigInCache(ctx, conf.Config, layerDescriptors)
	if err != nil {
		return client.ManifestRef{}, fmt.Errorf("unable to store config in cache: %w", err)
	}

	if err := bc.Client.Blob.Put(ctx, in, configDesc.Digest); err != nil {
		return client.ManifestRef{}, fmt.Errorf("unable to upload config blob: %w", err)
	}

	// create manifest and put it in cache
	manifest := manifest{
		SchemaVersion: 2, //nolint:gomnd // is a mmagic number from the spec
		Config:        configDesc,
		Layers:        layerDescriptors,
		MediaType:     "application/vnd.docker.distribution.manifest.v2+json",
	}

	manifestDigest, err := storeManifestInCache(ctx, manifest)
	if err != nil {
		return client.ManifestRef{}, fmt.Errorf("unable to write manifest: %w", err)
	}

	// push manifest
	pushRef := client.ManifestRef{ImageName: in, Tag: tagFor(ctx, bc, in, manifestDigest, bc.TagPrefix)}
	ref := pushRef
	ref.Digest = manifestDigest

	if err := bc.Client.Manifest.Head(ctx, pushRef); err == nil {
		return ref, nil
	}

	if err := bc.Client.Manifest.Put(ctx, in, pushRef, manifestDigest); err != nil {
		return client.ManifestRef{}, fmt.Errorf("unable to push manifest %s:%s: %w", in, manifestDigest.Encoded(), err)
	}

	return ref, nil
}
