// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"context"
	"fmt"
	"os"
	"path/filepath"

	"github.com/hashicorp/go-hclog"
	"github.com/spf13/cobra"
	"gitlab.com/thriqon/vinci"
	"gitlab.com/thriqon/vinci/pkg/cache"
	"gitlab.com/thriqon/vinci/pkg/client"
	"gitlab.com/thriqon/vinci/pkg/config"
)

func buildFile(ctx context.Context, bc vinci.BuildConfig, fname string) (client.ManifestRef, error) {
	logger := hclog.FromContext(ctx).With("build-file", fname)
	fileContext := hclog.WithContext(ctx, logger)

	f, err := os.Open(fname)
	if err != nil {
		return client.ManifestRef{}, fmt.Errorf("read build file %s: %w", fname, err)
	}

	defer f.Close()

	cfg, err := config.Load(ctx, f)
	if err != nil {
		return client.ManifestRef{}, fmt.Errorf("decode build file %s: %w", fname, err)
	}

	res, err := bc.BuildV1(fileContext, cfg)
	if err != nil {
		return client.ManifestRef{}, fmt.Errorf("build %s: %w", fname, err)
	}

	logger.Info("completed", "result", res)

	return res, nil
}

func buildCmd() *cobra.Command {
	var insecureRegistries []string
	var outputMethod outputMethodFlag
	var tagPrefix string

	cmd := &cobra.Command{
		Args:    cobra.MinimumNArgs(1),
		Use:     "build FILE...",
		Aliases: []string{"b"},
		Example: "vinci build hello-world.yaml",
		Run: func(cmd *cobra.Command, args []string) {
			logger := hclog.FromContext(cmd.Context())

			ch, err := cache.DefaultCache()
			if err != nil {
				logger.Error("create cache", "err", err)

				os.Exit(1)
			}

			cl := client.NewClient(ch, insecureRegistries, nil)
			refs := make([]client.ManifestRef, 0, len(args))

			for _, fname := range args {
				bc := vinci.BuildConfig{Client: cl, Cache: ch, TagPrefix: tagPrefix, BaseDir: filepath.Dir(fname)}

				ref, err := buildFile(cmd.Context(), bc, fname)
				if err != nil {
					logger.Error("build file", "file", fname, "err", err)

					os.Exit(1)
				}

				refs = append(refs, ref)
			}

			if err := outputMethod.render(os.Stdout, refs); err != nil {
				logger.Error("render", "err", err)

				os.Exit(1)
			}
		},
	}

	cmd.Flags().StringSliceVar(&insecureRegistries, "insecure-registry", []string{}, "Insecure Registries")
	cmd.Flags().VarP(&outputMethod, "output", "o", "Output format")
	cmd.Flags().StringVarP(&tagPrefix, "prefix", "p", "", "Prefix tags (if needed) with this string")

	return cmd
}
