// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"context"
	"os"
	"os/signal"
	"runtime"

	"github.com/hashicorp/go-hclog"
	"github.com/spf13/cobra"
)

func main() {
	logger := hclog.New(&hclog.LoggerOptions{
		Name:  "vinci",
		Color: hclog.AutoColor,
	})

	var logLevelDiff int

	rootCmd := &cobra.Command{
		Use:   "vinci",
		Short: "vinci - Masterful Image Management",

		PersistentPreRun: func(c *cobra.Command, args []string) {
			newLevel := int(hclog.Warn) - logLevelDiff

			// clamp to trace as minimal log level number
			if newLevel <= 0 {
				newLevel = int(hclog.Trace)
			}

			logger.SetLevel(hclog.Level(newLevel))
		},
	}

	rootCmd.PersistentFlags().CountVarP(&logLevelDiff, "verbose", "v", "Increase verbosity, up to three times")

	ctx, cancel := context.WithCancel(hclog.WithContext(context.Background(), logger))

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt)

	go func() {
		var buf []byte
		if logger.IsDebug() {
			buf = make([]byte, 1<<20) //nolint:gomnd // we just need a big number
		}
		<-signals
		if logger.IsDebug() {
			stacklen := runtime.Stack(buf, true)
			logger.Debug("stacktrace", "stacktrace", "\n"+string(buf[:stacklen]))
		}
		cancel()
	}()

	rootCmd.AddCommand(buildCmd())
	rootCmd.AddCommand(versionCmd())

	if err := rootCmd.ExecuteContext(ctx); err != nil {
		logger.Error(err.Error())
		os.Exit(1)
	}
}
