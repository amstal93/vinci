// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main //nolint:testpackage // Not exported functions

import (
	"bytes"
	"testing"

	"github.com/opencontainers/go-digest"
	"github.com/stretchr/testify/assert"
	"gitlab.com/thriqon/vinci/pkg/client"
)

func TestOmKustomize(t *testing.T) {
	t.Parallel()

	buf := bytes.Buffer{}
	err := omKustomize(&buf, []client.ManifestRef{
		{ImageName: client.ImageName{Host: "example.com", Name: "a/a"}, Tag: "v1"},
		{ImageName: client.ImageName{Host: "example.com", Name: "a/b"}, Digest: digest.FromString("asd")},
	})

	assert.NoError(t, err)
	assert.Equal(t, `images:
    - name: example.com/a/a
      newTag: v1
    - name: example.com/a/b
      digest: sha256:688787d8ff144c502c7f5cffaafe2cc588d86079f9de88304c26b0cb99ce91c6
`, buf.String())
}

func TestOmTable(t *testing.T) {
	t.Parallel()

	buf := bytes.Buffer{}
	err := omTable(&buf, []client.ManifestRef{
		{ImageName: client.ImageName{Host: "example.com", Name: "a/a"}, Tag: "v1"},
		{ImageName: client.ImageName{Host: "example.com", Name: "a/b"}, Digest: digest.FromString("asd")},
	})
	assert.NoError(t, err)
	assert.Equal(t, `IMAGE           TAG DIGEST
example.com/a/a v1  
example.com/a/b     sha256:688787d8ff144c502c7f5cffaafe2cc588d86079f9de88304c26b0cb99ce91c6
`, buf.String())
}
