// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"text/tabwriter"

	"gitlab.com/thriqon/vinci/pkg/client"
	yaml "gopkg.in/yaml.v3"
)

type outputMethod func(w io.Writer, images []client.ManifestRef) error

type outputMethodFlag struct {
	original string
	method   outputMethod
}

func (omf *outputMethodFlag) String() string {
	return omf.original
}

var errUnknownOutputMethod = errors.New("output method not known")

func (omf *outputMethodFlag) Set(orig string) error {
	omf.original = orig

	switch orig {
	case "kustomize":
		omf.method = omKustomize
	case "json":
		omf.method = omJSON
	case "table":
		omf.method = omTable
	case "list":
		omf.method = omList
	default:
		return fmt.Errorf("use output method %s: %w", orig, errUnknownOutputMethod)
	}

	return nil
}

func (omf *outputMethodFlag) render(w io.Writer, images []client.ManifestRef) error {
	if omf.method != nil {
		return omf.method(w, images)
	}

	return omList(w, images)
}

func (omf *outputMethodFlag) Type() string {
	return "output method"
}

func omKustomize(w io.Writer, images []client.ManifestRef) error {
	type imageInfo struct {
		Name   string `yaml:"name"`
		Tag    string `yaml:"newTag,omitempty"`
		Digest string `yaml:"digest,omitempty"`
	}

	data := struct {
		Images []imageInfo `yaml:"images"`
	}{}

	for _, ref := range images {
		data.Images = append(data.Images, imageInfo{
			Name:   ref.ImageName.String(),
			Tag:    ref.Tag,
			Digest: ref.Digest.String(),
		})
	}

	if err := yaml.NewEncoder(w).Encode(data); err != nil {
		return fmt.Errorf("encode as kustomize: %w", err)
	}

	return nil
}

func omJSON(w io.Writer, images []client.ManifestRef) error {
	type imageInfo struct {
		Name      string `json:"name"`
		Tag       string `json:"tag"`
		Digest    string `json:"digest"`
		Reference string `json:"reference"`
	}

	imageInfos := make([]imageInfo, 0, len(images))

	for _, ref := range images {
		imageInfos = append(imageInfos, imageInfo{
			Name:      ref.ImageName.String(),
			Tag:       ref.Tag,
			Digest:    ref.Digest.String(),
			Reference: ref.String(),
		})
	}

	data := struct {
		Images []imageInfo `json:"images"`
	}{imageInfos}

	if err := json.NewEncoder(w).Encode(data); err != nil {
		return fmt.Errorf("encode as json: %w", err)
	}

	return nil
}

func omTable(w io.Writer, images []client.ManifestRef) error {
	tw := tabwriter.NewWriter(w, 1, 0, 1, ' ', 0)
	if _, err := fmt.Fprintln(tw, "IMAGE\tTAG\tDIGEST"); err != nil {
		return fmt.Errorf("table header: %w", err)
	}

	for _, ref := range images {
		if _, err := fmt.Fprintf(tw, "%s\t%s\t%s\n", ref.ImageName.String(), ref.Tag, ref.Digest.String()); err != nil {
			return fmt.Errorf("table row: %w", err)
		}
	}

	return tw.Flush()
}

func omList(w io.Writer, images []client.ManifestRef) error {
	for _, ref := range images {
		if _, err := fmt.Fprintf(w, "%s\n", ref.String()); err != nil {
			return fmt.Errorf("row: %w", err)
		}
	}

	return nil
}
