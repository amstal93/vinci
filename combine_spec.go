// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package vinci

import (
	"context"
	"fmt"

	imagev1 "github.com/opencontainers/image-spec/specs-go/v1"
	configv1 "gitlab.com/thriqon/vinci/pkg/config/v1"
)

// CombinedSpec 'flattens' multiple combinable specs into one layer.  For
// example, this allows to copy a set of files in a shared layer.
type CombinedSpec []CombinableSpec

var _ Spec = CombinedSpec(nil)

func NewCombinedSpec(s []configv1.LayerSpec) (Spec, error) {
	specs := make([]CombinableSpec, len(s))
	for i := range s {
		s, err := AsSpec(s[i])
		if err != nil {
			return nil, err
		}

		cs, ok := s.(CombinableSpec)
		if !ok {
			return nil, fmt.Errorf("unable to use %v as combinable spec: %w", s, ErrUninterpretableValue)
		}

		specs[i] = cs
	}

	return CombinedSpec(specs), nil
}

// EnsureInCache calls all subspecs in turn and stores the resulting layer in
// the cache.
func (cs CombinedSpec) EnsureInCache(bc context.Context) ([]imagev1.Descriptor, error) {
	return ensureMultisInCache(bc, cs)
}
