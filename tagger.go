// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package vinci

import (
	"context"
	"fmt"
	"strings"

	hclog "github.com/hashicorp/go-hclog"
	digest "github.com/opencontainers/go-digest"
	"gitlab.com/thriqon/vinci/pkg/client"
)

func shortenedDigest(dgst digest.Digest) string {
	s := dgst.String()

	// TODO make configurable?
	return s[len(s)-8:]
}

func alreadyTagged(ctx context.Context, bc BuildConfig, in client.ImageName, shortened string) (string, error) {
	tags, err := bc.Client.Tags.List(ctx, in)
	if err != nil {
		return "", fmt.Errorf("list tags: %w", err)
	}

	for _, tag := range tags {
		if strings.HasSuffix(tag, shortened) {
			return tag, nil
		}
	}

	return "", nil
}

func tagFor(ctx context.Context, bc BuildConfig, in client.ImageName, dgst digest.Digest, tagPrefix string) string {
	logger := hclog.FromContext(ctx).With("tag-for", "image-name", in)

	shortened := shortenedDigest(dgst)

	oldTag, err := alreadyTagged(ctx, bc, in, shortened)
	if err != nil {
		logger.Warn("detect old tag, continue with fresh tag", "shortened", shortened, "digest", dgst.String(), "err", err)

		oldTag = ""
	}

	if oldTag != "" {
		return oldTag
	}

	return tagPrefix + shortened
}
