// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package vinci

import (
	"archive/tar"
	"compress/gzip"
	"context"

	// Digester wants to have this imported anonymously.
	_ "crypto/sha256"
	"encoding/json"
	"fmt"
	"io"

	digest "github.com/opencontainers/go-digest"
	imagev1 "github.com/opencontainers/image-spec/specs-go/v1"
	"gitlab.com/thriqon/vinci/pkg/cache"
)

func cacheJSONObject(ctx context.Context, obj interface{}) (digest.Digest, int64, error) {
	return writeToCache(ctx, func(wc cache.EntryWriter) error {
		return json.NewEncoder(wc).Encode(obj)
	})
}

func writeToCache(ctx context.Context, f func(wc cache.EntryWriter) error) (digest.Digest, int64, error) {
	wc, err := Cache(ctx).Create(ctx)
	if err != nil {
		return digest.Digest(""), int64(0), fmt.Errorf("unable to prepare cache write: %w", err)
	}

	if err := f(wc); err != nil {
		wc.Close()

		return digest.Digest(""), int64(0), fmt.Errorf("unable to write: %w", err)
	}

	if err := wc.Close(); err != nil {
		return digest.Digest(""), int64(0), fmt.Errorf("unable to close file in cache: %w", err)
	}

	if err := wc.Commit(); err != nil {
		return digest.Digest(""), int64(0), fmt.Errorf("unable to commit file in cache: %w", err)
	}

	return wc.Digest(), wc.Size(), nil
}

func readJSONInto(ctx context.Context, dgst digest.Digest, target interface{}) error {
	rc, _, err := Cache(ctx).Open(ctx, dgst)
	if err != nil {
		return fmt.Errorf("read %s: %w", dgst, err)
	}

	defer rc.Close()

	if err := json.NewDecoder(rc).Decode(target); err != nil {
		return fmt.Errorf("decode %s: %w", dgst, err)
	}

	return nil
}

func ensureMultisInCache(ctx context.Context, specs []CombinableSpec) ([]imagev1.Descriptor, error) {
	unzippedDigester := digest.SHA256.Digester()

	dig, size, err := writeToCache(ctx, func(wc cache.EntryWriter) error {
		if err := func() error {
			gzipper := gzip.NewWriter(wc)
			defer gzipper.Close()

			tarOutput := io.MultiWriter(gzipper, unzippedDigester.Hash())

			tw := tar.NewWriter(tarOutput)
			defer tw.Close()

			for i, spec := range specs {
				if err := spec.ProvideAsTarEntries(ctx, tw); err != nil {
					return fmt.Errorf("unable to add entries from layer spec %d (%v): %w", i, spec, err)
				}
			}

			return nil
		}(); err != nil {
			return err
		}

		if err := wc.SetMeta(ctx, LayerID, []byte(unzippedDigester.Digest())); err != nil {
			return fmt.Errorf("set layerid meta entry: %w", err)
		}

		return nil
	})
	if err != nil {
		return nil, err
	}

	return []imagev1.Descriptor{
		{
			// MediaType: imagev1.MediaTypeImageLayerGzip,
			MediaType: "application/vnd.docker.image.rootfs.diff.tar.gzip",
			Digest:    dig,
			Size:      size,
		},
	}, nil
}
