// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package vinci_test

import (
	"context"
	"net/http/httptest"
	"testing"

	"github.com/google/go-containerregistry/pkg/crane"
	"github.com/google/go-containerregistry/pkg/registry"
	"github.com/google/go-containerregistry/pkg/v1/random"
	digest "github.com/opencontainers/go-digest"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/thriqon/vinci"
	"gitlab.com/thriqon/vinci/pkg/cache"
	"gitlab.com/thriqon/vinci/pkg/client"
)

func iosTestContext(h *httptest.Server) context.Context {
	ch := cache.NewMemCache()
	cl := client.NewClient(ch, []string{}, h.Client().Transport)

	return vinci.BuildContextFromConfig(context.Background(), vinci.BuildConfig{
		Cache:           ch,
		Client:          cl,
		ParallelWorkers: 1,
	})
}

func testRegistry(t *testing.T) *httptest.Server {
	t.Helper()

	reg, err := registry.TLS("test.getvinci.dev")
	require.NoError(t, err)

	return reg
}

func TestImportOCISpec(t *testing.T) {
	t.Parallel()

	reg := testRegistry(t)
	defer reg.Close()

	testimg, err := random.Image(1024, 5)
	require.NoError(t, err)

	dgst, err := testimg.Digest()
	require.NoError(t, err)

	layers, err := testimg.Layers()
	require.NoError(t, err)

	err = crane.Push(testimg, "test.getvinci.dev/test/a:latest", crane.WithTransport(reg.Client().Transport))
	require.NoError(t, err)

	c := iosTestContext(reg)

	ios, err := vinci.NewImportOCISpecV1("test.getvinci.dev/test/a:latest@" + dgst.String())
	require.NoError(t, err)

	descs, err := ios.EnsureInCache(c)
	require.NoError(t, err)

	assert.Len(t, descs, len(layers))

	for _, l := range layers {
		ld, err := l.Digest()
		assert.NoError(t, err)

		isCached, err := vinci.Cache(c).IsCached(c, digest.Digest(ld.String()))
		assert.NoError(t, err)
		assert.True(t, isCached)
	}
}

func TestImportOCISpec_FailParse(t *testing.T) {
	t.Parallel()

	_, err := vinci.NewImportOCISpecV1("bash:1.0")
	assert.Error(t, err, "must contain digest")

	_, err = vinci.NewImportOCISpecV1("/")
	assert.Error(t, err, "must be parseable as image name")
}
