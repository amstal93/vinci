// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package vinci

import (
	"context"
	"fmt"

	"github.com/mitchellh/hashstructure"
	imagev1 "github.com/opencontainers/image-spec/specs-go/v1"
	"gitlab.com/thriqon/vinci/pkg/client"
	configv1 "gitlab.com/thriqon/vinci/pkg/config/v1"
	"golang.org/x/sync/errgroup"
)

const LayerID = "layerID"

func (bc BuildConfig) uploadLayers(ctx context.Context, in client.ImageName, descs []imagev1.Descriptor) error {
	if err := AcquireToken(ctx, "upload/"+in.String()); err != nil {
		return err
	}
	defer ReleaseToken(ctx)

	// TODO wrong method name for this?
	for _, desc := range descs {
		if err := bc.Client.Blob.Head(ctx, in, desc.Digest); err == nil {
			continue
		}

		if err := bc.Client.Blob.Put(ctx, in, desc.Digest); err != nil {
			return fmt.Errorf("upload blob %s for %s: %w", desc.Digest, in, err)
		}
	}

	return nil
}

func (bc BuildConfig) buildV1Layers(ctx context.Context, specs []configv1.LayerSpec, in client.ImageName) ([]imagev1.Descriptor, error) {
	results := make([][]imagev1.Descriptor, len(specs))

	errg, ctx := errgroup.WithContext(ctx)

	for i := range specs {
		i := i
		spec, err := AsSpec(specs[i])
		if err != nil {
			return nil, fmt.Errorf("unable to use as spec: %w", err)
		}

		errg.Go(func() error {
			specHash, err := hashstructure.Hash(spec, nil)
			if err != nil {
				panic(err)
			}

			ctx = logWith(ctx, "spec-id", specHash)

			L(ctx).Debug("cache-layers")

			descs, err := spec.EnsureInCache(ctx)
			if err != nil {
				return fmt.Errorf("cache %d: %w", specHash, err)
			}

			results[i] = descs

			L(ctx).Debug("upload-layers", "descriptors", results[i])

			err = bc.uploadLayers(ctx, in, descs)

			L(ctx).Debug("upload-complete")

			return err
		})
	}

	// wait for complete pool
	if err := errg.Wait(); err != nil {
		return nil, fmt.Errorf("layer building: %w", err)
	}

	descriptors := []imagev1.Descriptor{}
	for _, descs := range results {
		descriptors = append(descriptors, descs...)
	}

	return descriptors, nil
}
