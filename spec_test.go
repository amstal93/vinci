// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package vinci_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/thriqon/vinci"
	configv1 "gitlab.com/thriqon/vinci/pkg/config/v1"
)

func TestAsSpec(t *testing.T) {
	t.Parallel()

	t.Run("empty spec value", func(t *testing.T) {
		t.Parallel()

		cs := configv1.LayerSpec{}

		spec, err := vinci.AsSpec(cs)
		assert.ErrorIs(t, err, vinci.ErrUninterpretableValue)
		assert.Nil(t, spec)
	})

	t.Run("uncombinable spec", func(t *testing.T) {
		t.Parallel()

		ociURL := "asd@sha256:688787d8ff144c502c7f5cffaafe2cc588d86079f9de88304c26b0cb99ce91c6"
		cs := configv1.LayerSpec{
			Combine: []configv1.LayerSpec{
				{ImportOCI: &ociURL},
			},
		}

		spec, err := vinci.AsSpec(cs)
		assert.ErrorIs(t, err, vinci.ErrUninterpretableValue)
		assert.Nil(t, spec)
	})

	t.Run("unparseable nested spec", func(t *testing.T) {
		t.Parallel()

		cs := configv1.LayerSpec{
			Combine: []configv1.LayerSpec{{}},
		}

		spec, err := vinci.AsSpec(cs)
		assert.ErrorIs(t, err, vinci.ErrUninterpretableValue)
		assert.Nil(t, spec)
	})
}
