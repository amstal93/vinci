// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package vinci_test

import (
	"context"
	"io/ioutil"
	"path/filepath"
	"testing"

	digest "github.com/opencontainers/go-digest"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/thriqon/vinci"
	"gitlab.com/thriqon/vinci/pkg/cache"
	"gitlab.com/thriqon/vinci/pkg/client"
)

func isTestContext() context.Context {
	ch := cache.NewMemCache()
	cl := client.NewClient(ch, []string{}, nil)

	return vinci.BuildContextFromConfig(context.Background(), vinci.BuildConfig{
		Cache:           ch,
		Client:          cl,
		ParallelWorkers: 1,
	})
}

func TestImportSpec_MissingFile(t *testing.T) {
	t.Parallel()
	t.Skip("unclear whether this is correct")

	_, err := vinci.NewImportSpecV1(filepath.Join(t.TempDir(), "missing"))
	require.Error(t, err)
}

func TestImportSpec(t *testing.T) {
	t.Parallel()

	c := isTestContext()
	require.NotNil(t, c)

	fname := filepath.Join(t.TempDir(), "a.yml")
	require.NoError(t, writeBaseYaml(fname))

	spec, err := vinci.NewImportSpecV1(fname)
	require.NoError(t, err)

	descs, err := spec.EnsureInCache(c)
	require.NoError(t, err)

	assert.Len(t, descs, 2)
	assert.Equal(t, digest.Digest("sha256:b68136941d5475e09f249d58c2a81a75e6c186cea2c914dfebf9ae5699f387b6"), descs[0].Digest)
	assert.Equal(t, digest.Digest("sha256:4b40210d2ef0b3f2680af71d2dfcb2757e0dacc00f024f9bd7c10f88512fb610"), descs[1].Digest)
}

func writeBaseYaml(fname string) error {
	return ioutil.WriteFile(fname, []byte(`
spec-version: v1

image: illegal.invalid

config:
  entrypoint:
    - /cat
    - /message

layers:
  - combine:
    - file:
        path: /etc/passwd
        mode: 0444
        contents: |
          root:x:0:0:root:/root:/bin/bash
    - file:
        path: /etc/group
        mode: 0444
        contents: |
          root:x:0:
  - file:
      path: /cat
      mode: 0555
      contents: |
        asd
`), 0o600)
}
