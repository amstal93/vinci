// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package vinci

import (
	"context"
	"errors"
	"fmt"
	"runtime"

	digest "github.com/opencontainers/go-digest"
	imagev1 "github.com/opencontainers/image-spec/specs-go/v1"
	"gitlab.com/thriqon/vinci/pkg/client"
	"golang.org/x/sync/errgroup"
)

var (
	ErrReferenceNotByDigest = errors.New("reference not by digest")
	ErrCorruptImportedImage = errors.New("corrupt import image")
	ErrNoCompatibleImage    = errors.New("no compatible image found")
)

const (
	MediaTypeImageIndexDockerVendor = "application/vnd.docker.distribution.manifest.list.v2+json"
	MediaTypeConfigDockerVendor     = "application/vnd.docker.container.image.v1+json"
)

type ImportOCISpec struct {
	Ref client.ManifestRef
}

var _ Spec = ImportOCISpec{}

func NewImportOCISpecV1(ref string) (Spec, error) {
	parsed, err := client.ParseManifestRef(ref)
	if err != nil {
		return nil, fmt.Errorf("import %s: %w", ref, err)
	}

	if !parsed.HasDigest() {
		return nil, fmt.Errorf("import %s: %w", ref, err)
	}

	return ImportOCISpec{Ref: parsed}, nil
}

func (ios ImportOCISpec) ifNotCached(ctx context.Context, dgst digest.Digest, f func() error) error {
	ok, err := Cache(ctx).IsCached(ctx, dgst)
	if err != nil {
		return fmt.Errorf("cache-test: %w", err)
	}

	if !ok {
		return f()
	}

	return nil
}

func (ios ImportOCISpec) getManifestFromIndex(ctx context.Context, ref digest.Digest) (digest.Digest, bool, error) {
	L(ctx).Trace("parse image index", "ref", ref)

	var imageIndex imagev1.Index
	if err := readJSONInto(ctx, ref, &imageIndex); err != nil {
		return digest.Digest(""), false, fmt.Errorf("parse image index: %w", err)
	}

	for _, desc := range imageIndex.Manifests {
		if desc.Platform == nil {
			L(ctx).Trace("ignore manifest since platform=nil", "digest", desc.Digest)

			continue
		}

		L(ctx).Trace("considering manifest", "arch", desc.Platform.Architecture, "os", desc.Platform.OS, "digest", desc.Digest)

		if desc.Platform.Architecture == runtime.GOARCH && desc.Platform.OS == runtime.GOOS {
			return desc.Digest, true, nil
		}
	}

	return digest.Digest(""), false, nil
}

func (ios ImportOCISpec) cacheManifest(ctx context.Context, dig digest.Digest) error {
	return ios.ifNotCached(ctx, dig, func() error {
		L(ctx).Info("get manifest", "ref", ios.Ref, "digest", dig)

		ref := client.ManifestRef{ImageName: ios.Ref.ImageName, Digest: dig}

		if _, err := Client(ctx).Manifest.Get(ctx, ref); err != nil {
			return fmt.Errorf("GET manifest: %w", err)
		}

		return nil
	})
}

func (ios ImportOCISpec) getManifest(ctx context.Context) (imagev1.Manifest, error) {
	if err := AcquireToken(ctx, "get-manifest/"+ios.Ref.String()); err != nil {
		return imagev1.Manifest{}, err
	}
	defer ReleaseToken(ctx)

	// fetch manifest
	if err := ios.cacheManifest(ctx, ios.Ref.Digest); err != nil {
		return imagev1.Manifest{}, fmt.Errorf("get manifest %s: %w", ios.Ref, err)
	}
	manifestDigest := ios.Ref.Digest

	mediaTypeB, err := Cache(ctx).ReadMeta(ctx, manifestDigest, client.ContentType)
	if err != nil {
		return imagev1.Manifest{}, fmt.Errorf("get content-type of %s: %w", manifestDigest, err)
	}

	mediaType := string(mediaTypeB)

	L(ctx).Debug("manifest type", "ref", ios.Ref, "type", mediaType)

	if mediaType == imagev1.MediaTypeImageIndex || mediaType == MediaTypeImageIndexDockerVendor {
		// get manifest from index
		var found bool

		manifestDigest, found, err = ios.getManifestFromIndex(ctx, ios.Ref.Digest)
		if err != nil {
			return imagev1.Manifest{}, err
		}

		if !found {
			return imagev1.Manifest{}, fmt.Errorf("search for %s/%s: %w", runtime.GOARCH, runtime.GOOS, ErrNoCompatibleImage)
		}

		if err := ios.cacheManifest(ctx, manifestDigest); err != nil {
			return imagev1.Manifest{}, fmt.Errorf("importing platform manifest for %s on %s/%s: %w", ios.Ref, runtime.GOARCH, runtime.GOOS, err)
		}
	}

	// identify image config
	var manifest imagev1.Manifest
	if err := readJSONInto(ctx, manifestDigest, &manifest); err != nil {
		return imagev1.Manifest{}, fmt.Errorf("read manifest: %w", err)
	}

	return manifest, nil
}

func (ios ImportOCISpec) getConfig(ctx context.Context, manifest imagev1.Manifest) (imagev1.Image, error) {
	if err := AcquireToken(ctx, "get-config/"+ios.Ref.String()); err != nil {
		return imagev1.Image{}, err
	}
	defer ReleaseToken(ctx)

	var configDescriptor imagev1.Descriptor = manifest.Config

	if configDescriptor.MediaType != imagev1.MediaTypeImageConfig && configDescriptor.MediaType != MediaTypeConfigDockerVendor {
		return imagev1.Image{}, fmt.Errorf("image config media type %s in manifest %s: %w", configDescriptor.MediaType, ios.Ref, ErrCorruptImportedImage)
	}

	// fetch image config
	if err := ios.ifNotCached(ctx, configDescriptor.Digest, func() error {
		err := Client(ctx).Blob.Get(ctx, ios.Ref.ImageName, configDescriptor.Digest)
		if err != nil {
			return fmt.Errorf("GET config %s: %w", configDescriptor.Digest, err)
		}

		return nil
	}); err != nil {
		return imagev1.Image{}, fmt.Errorf("importing config for %s: %w", ios.Ref, err)
	}

	// identify layers
	var image imagev1.Image
	if err := readJSONInto(ctx, configDescriptor.Digest, &image); err != nil {
		return imagev1.Image{}, fmt.Errorf("read image config: %w", err)
	}

	return image, nil
}

func (ios ImportOCISpec) EnsureInCache(ctx context.Context) ([]imagev1.Descriptor, error) {
	logger := L(ctx).With("ref", ios.Ref)
	logger.Debug("import into cache")

	manifest, err := ios.getManifest(ctx)
	if err != nil {
		return nil, err
	}

	image, err := ios.getConfig(ctx, manifest)
	if err != nil {
		return nil, err
	}

	// fetch each layer and store in cache
	errg, ctx := errgroup.WithContext(ctx)

	for i := range manifest.Layers {
		i := i
		errg.Go(func() error {
			if err := AcquireToken(ctx, "fetch-layer/"+ios.Ref.String()); err != nil {
				return err
			}
			defer ReleaseToken(ctx)

			logger.Trace("caching blob", "layer-id", manifest.Layers[i].Digest, "image-name", ios.Ref.ImageName)

			return ios.ifNotCached(ctx, manifest.Layers[i].Digest, func() error {
				logger.Trace("retrieving blob", "layer-id", manifest.Layers[i].Digest, "image-name", ios.Ref.ImageName)

				err := Client(ctx).Blob.Get(ctx, ios.Ref.ImageName, manifest.Layers[i].Digest, client.Metadata(LayerID, []byte(image.RootFS.DiffIDs[i].String())))
				if err != nil {
					return fmt.Errorf("GET blob for %s: %w", ios.Ref, err)
				}

				return nil
			})
		})
	}

	return manifest.Layers, errg.Wait()
}
