// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package client

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"sort"

	"github.com/google/go-containerregistry/pkg/v1/remote/transport"
	"github.com/hashicorp/go-hclog"
	"github.com/mitchellh/hashstructure"
)

type TagService struct {
	client *Client
}

func (ts *TagService) List(ctx context.Context, n ImageName) ([]string, error) {
	logger := hclog.FromContext(ctx)

	cl, err := ts.client.httpClientFor(n, transport.PullScope)
	if err != nil {
		return nil, fmt.Errorf("get client for %s: %w", n, err)
	}
	base := n.AsURLPrefix() + "/tags/list"

	tags := make(map[string]struct{})

	last := ""
	oldHash, err := hashstructure.Hash(tags, nil)
	if err != nil {
		panic(err)
	}

	for {
		u := base + "?n=500"
		if last != "" {
			u += "&last=" + last
		}

		batch, err := ts.loadBatch(ctx, cl, logger, u)
		if err != nil {
			return nil, err
		}

		for _, newTag := range batch {
			tags[newTag] = struct{}{}
		}

		newHash, err := hashstructure.Hash(tags, nil)
		if err != nil {
			panic(err)
		}

		logger.Trace("after batch", "old", oldHash, "new", newHash, "len-tags", len(tags))

		if newHash == oldHash {
			break
		}

		oldHash = newHash
		last = batch[len(batch)-1]
	}

	res := make([]string, 0, len(tags))
	for k := range tags {
		res = append(res, k)
	}

	sort.Strings(res)

	return res, nil
}

func (ts *TagService) loadBatch(ctx context.Context, cl *http.Client, logger hclog.Logger, u string) ([]string, error) {
	logger.Debug("GET", "url", u)
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, u, nil)
	if err != nil {
		return nil, fmt.Errorf("prepare %s: %w", u, err)
	}

	res, err := cl.Do(req)
	if err != nil {
		return nil, fmt.Errorf("get %s: %w", u, err)
	}

	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		logger.Info("got no tags for repository", "url", u, "status", res.Status)

		return []string{}, nil
	}

	data := struct {
		Tags []string `json:"tags"`
	}{}

	if err := json.NewDecoder(res.Body).Decode(&data); err != nil {
		return nil, fmt.Errorf("decode json: %w", err)
	}

	return data.Tags, nil
}
