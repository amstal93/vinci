// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package client

import (
	"errors"
	"fmt"
	"regexp"
	"strings"

	"github.com/google/go-containerregistry/pkg/authn"
	digest "github.com/opencontainers/go-digest"
)

const dockerHubHost = "index.docker.io"

type ImageName struct {
	Host string
	Name string
}

var (
	imageNameComponent = regexp.MustCompile("^[a-z0-9]+(?:[._-][a-z0-9]+)*$")

	ErrImageNameInvalid = errors.New("image name is of the wrong format")
)

func ParseImageName(n string) (ImageName, error) {
	parts := strings.Split(n, "/")

	return parseImageNameParts(parts)
}

func parseImageNameParts(parts []string) (ImageName, error) {
	switch {
	case len(parts) == 1:
		if !imageNameComponent.MatchString(parts[0]) {
			return ImageName{}, fmt.Errorf("unable to parse name %s as image name: %w", parts[0], ErrImageNameInvalid)
		}

		return ImageName{dockerHubHost, "library/" + parts[0]}, nil

	case len(parts) == 2: //nolint:gomnd // Two is not a magic number here, but simply 1+1.
		if !imageNameComponent.MatchString(parts[0]) || !imageNameComponent.MatchString(parts[1]) {
			return ImageName{}, fmt.Errorf("unable to parse name (%v) as image name: %w", parts, ErrImageNameInvalid)
		}

		return ImageName{dockerHubHost, parts[0] + "/" + parts[1]}, nil

	default:
		for _, p := range parts[1:] {
			if !imageNameComponent.MatchString(p) {
				return ImageName{}, fmt.Errorf("unable to parse component '%s' in image name (%v): %w", p, parts, ErrImageNameInvalid)
			}
		}

		return ImageName{parts[0], strings.Join(parts[1:], "/")}, nil
	}
}

func (in ImageName) String() string {
	return in.Host + "/" + in.Name
}

func (in ImageName) RegistryStr() string {
	return in.Host
}

var _ authn.Resource = ImageName{}

func (in ImageName) AsURLPrefix() string {
	return fmt.Sprintf("https://%s/v2/%s", in.Host, in.Name)
}

func (in ImageName) RegistryPrefix() string {
	return fmt.Sprintf("https://%s/v2/", in.Host)
}

type ManifestRef struct {
	ImageName
	Tag    string
	Digest digest.Digest
}

func ParseManifestRef(r string) (ManifestRef, error) {
	parts := strings.Split(r, "/")

	tag := ""
	dig := digest.Digest("")

	if idx := strings.LastIndexByte(parts[len(parts)-1], '@'); idx >= 0 {
		dig = digest.Digest(parts[len(parts)-1][idx+1:])

		parts[len(parts)-1] = parts[len(parts)-1][0:idx]

		if err := dig.Validate(); err != nil {
			return ManifestRef{}, fmt.Errorf("parse %s: %w", r, err)
		}
	}

	if idx := strings.IndexRune(parts[len(parts)-1], ':'); idx >= 0 {
		tag = parts[len(parts)-1][idx+1:]
		parts[len(parts)-1] = parts[len(parts)-1][0:idx]
	}

	in, err := parseImageNameParts(parts)
	if err != nil {
		return ManifestRef{}, err
	}

	return ManifestRef{ImageName: in, Tag: tag, Digest: dig}, nil
}

func (mr ManifestRef) String() string {
	in := mr.ImageName.String()

	if mr.Tag != "" {
		in += ":" + mr.Tag
	}

	if mr.Digest != "" {
		in += "@" + mr.Digest.String()
	}

	return in
}

func (mr ManifestRef) ManifestURL() string {
	selector := "latest"

	if mr.Digest != "" {
		selector = mr.Digest.String()
	} else if mr.Tag != "" {
		selector = mr.Tag
	}

	return fmt.Sprintf("https://%s/v2/%s/manifests/%s", mr.Host, mr.Name, selector)
}

func (mr ManifestRef) HasDigest() bool {
	return mr.Digest != ""
}
