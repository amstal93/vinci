// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package client

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/google/go-containerregistry/pkg/authn"
	"github.com/google/go-containerregistry/pkg/name"
	"github.com/google/go-containerregistry/pkg/v1/remote/transport"
	"gitlab.com/thriqon/vinci/pkg/cache"
)

var (
	ErrUnexpectedStatusCode    = errors.New("unexpected status code")
	ErrInvalidDigestFromServer = errors.New("server sent invalid digest")

	// ErrNotRegistryV2 is returned when the endpoint indicated by the image name
	// does not respond to a ping to /v2/ as expected.
	ErrNotRegistryV2 = errors.New("endpoint is not a registry with version v2")
)

type addr string

type Client struct {
	Manifest *ManifestService
	Blob     *BlobService
	Tags     *TagService

	blobCache          cache.Cache
	kc                 authn.Keychain
	insecureRegistries map[addr]struct{}

	underlyingTransport http.RoundTripper
}

// NewClient builds a new client.
func NewClient(c cache.Cache, insecureRegistries []string, transport http.RoundTripper) *Client {
	ir := make(map[addr]struct{})
	for _, r := range insecureRegistries {
		ir[addr(r)] = struct{}{}
	}

	cl := Client{
		blobCache:           c,
		insecureRegistries:  ir,
		kc:                  authn.DefaultKeychain,
		underlyingTransport: newVinciTransport(transport),
	}

	cl.Manifest = &ManifestService{&cl}
	cl.Blob = &BlobService{&cl}
	cl.Tags = &TagService{&cl}

	return &cl
}

func (c *Client) asRepository(n ImageName) (name.Repository, error) {
	options := []name.Option{}

	if _, ok := c.insecureRegistries[addr(n.Host)]; ok {
		options = append(options, name.Insecure)
	}

	repo, err := name.NewRepository(n.String(), options...)
	if err != nil {
		return repo, fmt.Errorf("unable to treat %s / %s as repository: %w", n.Host, n.Name, err)
	}

	return repo, nil
}

func (c *Client) httpClientFor(n ImageName, scope string) (*http.Client, error) {
	auth, err := authn.DefaultKeychain.Resolve(n)
	if err != nil {
		return nil, fmt.Errorf("unable to resolve authenticator for %v: %w", n, err)
	}

	repo, err := c.asRepository(n)
	if err != nil {
		return nil, err
	}

	tp, err := transport.New(repo.Registry, auth, c.underlyingTransport, []string{repo.Scope(scope)})
	if err != nil {
		return nil, fmt.Errorf("unable to get transport for %v for scope %v: %w", n, scope, err)
	}

	return &http.Client{Transport: tp}, nil
}
