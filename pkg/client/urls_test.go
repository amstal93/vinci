// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package client_test

import (
	"testing"

	digest "github.com/opencontainers/go-digest"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/thriqon/vinci/pkg/client"
)

func TestParseImageName(t *testing.T) {
	t.Parallel()

	data := []struct {
		n  string
		in client.ImageName
	}{
		{"bash", client.ImageName{"index.docker.io", "library/bash"}},
		{"a/b", client.ImageName{"index.docker.io", "a/b"}},
		{"quay.io/a/b", client.ImageName{"quay.io", "a/b"}},
		{"localhost:5000/asd/asd", client.ImageName{"localhost:5000", "asd/asd"}},
	}

	for _, test := range data {
		test := test
		t.Run(test.n, func(t *testing.T) {
			t.Parallel()

			actual, err := client.ParseImageName(test.n)
			assert.NoError(t, err)
			assert.Equal(t, test.in, actual)
		})
	}
}

func TestParseImageName_Invalid(t *testing.T) {
	t.Parallel()

	data := []string{
		"bash:latest",
		"bash@sha245:12391231=23",
		"bash:latest@sha283:3023423481-2",

		"localhost:5000/asd/!!!",
		"localhost:5000/!!!/asd",
		"localhost:5000/asd",
		"localhost:5000",

		"!2",
		"///",
		"",
	}

	for _, test := range data { //nolint:paralleltest // Bug in linter?
		test := test
		t.Run(test, func(t *testing.T) {
			t.Parallel()

			_, err := client.ParseImageName(test)
			assert.Error(t, err)
		})
	}
}

func TestParseManifestRef(t *testing.T) {
	t.Parallel()

	digA := digest.FromString("a")
	digB := digest.FromString("b")

	data := []struct {
		n  string
		in client.ManifestRef
	}{
		{"bash", client.ManifestRef{client.ImageName{"index.docker.io", "library/bash"}, "", digest.Digest("")}},
		{"a/b", client.ManifestRef{client.ImageName{"index.docker.io", "a/b"}, "", digest.Digest("")}},
		{"quay.io/a/b", client.ManifestRef{client.ImageName{"quay.io", "a/b"}, "", digest.Digest("")}},
		{"localhost:5000/asd/asd", client.ManifestRef{client.ImageName{"localhost:5000", "asd/asd"}, "", digest.Digest("")}},

		{"bash:latest", client.ManifestRef{client.ImageName{"index.docker.io", "library/bash"}, "latest", digest.Digest("")}},
		{"a/b:v1", client.ManifestRef{client.ImageName{"index.docker.io", "a/b"}, "v1", digest.Digest("")}},
		{"quay.io/a/b:v2", client.ManifestRef{client.ImageName{"quay.io", "a/b"}, "v2", digest.Digest("")}},
		{"localhost:5000/asd/asd:v3", client.ManifestRef{client.ImageName{"localhost:5000", "asd/asd"}, "v3", digest.Digest("")}},
		{"bash:v1@" + digA.String(), client.ManifestRef{client.ImageName{"index.docker.io", "library/bash"}, "v1", digA}},
		{"a/b:latest@" + digA.String(), client.ManifestRef{client.ImageName{"index.docker.io", "a/b"}, "latest", digA}},
		{"quay.io/a/b:v2@" + digB.String(), client.ManifestRef{client.ImageName{"quay.io", "a/b"}, "v2", digB}},
		{"localhost:5000/asd/asd@" + digB.String(), client.ManifestRef{client.ImageName{"localhost:5000", "asd/asd"}, "", digB}},

		{"bash@" + digA.String(), client.ManifestRef{client.ImageName{"index.docker.io", "library/bash"}, "", digA}},
		{"a/b@" + digA.String(), client.ManifestRef{client.ImageName{"index.docker.io", "a/b"}, "", digA}},
		{"quay.io/a/b@" + digB.String(), client.ManifestRef{client.ImageName{"quay.io", "a/b"}, "", digB}},
		{"localhost:5000/asd/asd@" + digB.String(), client.ManifestRef{client.ImageName{"localhost:5000", "asd/asd"}, "", digB}},
	}

	for _, test := range data {
		test := test
		t.Run(test.n, func(t *testing.T) {
			t.Parallel()

			actual, err := client.ParseManifestRef(test.n)
			assert.NoError(t, err)
			assert.Equal(t, test.in, actual)
		})
	}
}

func TestParseManifestRef_InvalidDigest(t *testing.T) {
	t.Parallel()

	_, err := client.ParseManifestRef("test/a@sha123:1239012323")
	assert.Error(t, err)
}

func TestParseManifestRef_URL(t *testing.T) {
	t.Parallel()

	refS := "quay.io/a/b:v2@sha256:ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb"
	ref, err := client.ParseManifestRef(refS)
	require.NoError(t, err)

	assert.Equal(t, ref.ManifestURL(), "https://quay.io/v2/a/b/manifests/sha256:ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb")

	refS = "quay.io/a/b:v2"
	ref, err = client.ParseManifestRef(refS)
	require.NoError(t, err)

	assert.Equal(t, ref.ManifestURL(), "https://quay.io/v2/a/b/manifests/v2")
}
