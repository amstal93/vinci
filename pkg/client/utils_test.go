// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package client_test

import (
	"context"
	"testing"

	digest "github.com/opencontainers/go-digest"
	"github.com/stretchr/testify/require"
	"gitlab.com/thriqon/vinci/pkg/cache"
)

func newTestCache(t *testing.T) cache.Cache {
	t.Helper()

	return cache.InDirectory(t.TempDir())
}

func Store(t *testing.T, tc cache.Cache, bs []byte, v ...interface{}) digest.Digest {
	t.Helper()

	ew, err := tc.Create(context.Background())
	require.NoError(t, err)

	_, err = ew.Write(bs)
	require.NoError(t, err)

	err = ew.Close()
	require.NoError(t, err)

	for i := 0; i < len(v)/2; i += 2 {
		k := v[i].(string)
		v := v[i+1].([]byte)

		require.NoError(t, ew.SetMeta(context.Background(), k, v))
	}

	err = ew.Commit()
	require.NoError(t, err)

	return ew.Digest()
}
