// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package client_test

import (
	"context"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	digest "github.com/opencontainers/go-digest"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/thriqon/vinci/pkg/client"
)

func TestBlobPut_Mounted(t *testing.T) {
	t.Parallel()

	// given
	dbc := newTestCache(t)

	var asd digest.Digest
	var baseName string

	usedBlob := false

	mux := http.NewServeMux()
	mux.HandleFunc("/v2/image/a/blobs/uploads/", func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, http.MethodPost, r.Method)
		assert.Equal(t, asd.String(), r.URL.Query().Get("mount"))
		assert.Equal(t, baseName+"/image/original", r.URL.Query().Get("from"))
		w.WriteHeader(http.StatusCreated)

		usedBlob = true
	})

	mux.HandleFunc("/v2/", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, http.MethodGet, r.Method)
	})

	server := httptest.NewTLSServer(mux)
	baseName = server.Listener.Addr().String()

	defer server.Close()

	asd = Store(t, dbc, []byte("asd"),
		client.MetaRepository, []byte(baseName+"/image/original"))

	// when
	cl := client.NewClient(dbc, nil, server.Client().Transport)

	in, err := client.ParseImageName(baseName + "/image/a")
	require.NoError(t, err)

	err = cl.Blob.Put(context.Background(), in, asd)
	require.NoError(t, err)

	// then
	assert.True(t, usedBlob, "Indicates whether blob was used")
}

func TestBlobPut_OtherSourceRepo(t *testing.T) {
	t.Parallel()

	// given
	dbc := newTestCache(t)

	var baseName string

	normalBlob := false

	mux := http.NewServeMux()

	mux.HandleFunc("/v2/image/other/blobs/uploads/", func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, http.MethodPost, r.Method)
		assert.Equal(t, "", r.URL.Query().Get("mount"))
		assert.Equal(t, "", r.URL.Query().Get("from"))
		w.WriteHeader(http.StatusNotFound)

		normalBlob = true
	})

	mux.HandleFunc("/v2/", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, http.MethodGet, r.Method)
	})

	server := httptest.NewTLSServer(mux)
	baseName = server.Listener.Addr().String()

	defer server.Close()

	other := Store(t, dbc, []byte("other"),
		client.MetaRepository, []byte("example.invalid/image/original"))

	// when
	cl := client.NewClient(dbc, nil, server.Client().Transport)

	// push other image (not fully mocked)
	in, err := client.ParseImageName(baseName + "/image/other")
	require.NoError(t, err)

	err = cl.Blob.Put(context.Background(), in, other)
	require.Error(t, err) // push won't work, not fully mocked.

	// then
	assert.True(t, normalBlob, "Indicates whether unmountable blob was used")
}

func TestBlobPut(t *testing.T) { //nolint:funlen // long test case.
	t.Parallel()

	// given
	dbc := newTestCache(t)

	asd := Store(t, dbc, []byte("asd"))

	rand.Seed(time.Now().UnixNano())
	sessionID := rand.Int() //nolint:gosec // Not security relevant, this is just a test.
	sessionURL := fmt.Sprintf("/opaque/session-url/%d", sessionID)

	t.Logf("using sessionURL=%s", sessionURL)

	uploadBlob := false

	mux := http.NewServeMux()
	mux.HandleFunc("/v2/image/a/blobs/uploads/", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, http.MethodPost, r.Method)
		// Test transporting a server-supplied state parameter.
		w.Header().Set("Location", sessionURL+"?state=asd")
		w.Header().Set("Range", "0-0")
		w.Header().Set("Blob-Upload-Session-ID", fmt.Sprintf("%d", sessionID))
		w.WriteHeader(http.StatusAccepted)
	})

	mux.HandleFunc(sessionURL, func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, http.MethodPut, r.Method)
		require.Equal(t, "3", r.Header.Get("Content-Length"), "Content-Length Header")
		require.Equal(t, "application/octet-stream", r.Header.Get("Content-Type"))

		bs, err := ioutil.ReadAll(r.Body)
		require.NoError(t, err)
		require.Equal(t, "asd", string(bs))

		require.Equal(t, asd.String(), r.URL.Query().Get("digest"))
		require.Equal(t, "asd", r.URL.Query().Get("state"))

		w.Header().Set("Location", "/v2/image/a/blobs/"+asd.String())
		w.Header().Set("Content-Range", "0-2")
		w.Header().Set("Docker-Content-Digest", asd.String())
		w.WriteHeader(http.StatusNoContent)

		assert.False(t, uploadBlob)
		uploadBlob = true
	})
	mux.HandleFunc("/v2/", func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, http.MethodGet, r.Method)
	})

	server := httptest.NewTLSServer(mux)

	defer server.Close()

	// when
	cl := client.NewClient(dbc, nil, server.Client().Transport)

	in, err := client.ParseImageName(server.Listener.Addr().String() + "/image/a")
	require.NoError(t, err)

	err = cl.Blob.Put(context.Background(), in, asd)
	require.NoError(t, err)

	// then
	assert.True(t, uploadBlob, "Indicates whether blob was uploaded")
}

func TestBlobGet(t *testing.T) {
	t.Parallel()

	// given
	dbc := newTestCache(t)

	asd := digest.FromString("asd")

	calledServer := 0
	server := httptest.NewTLSServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodGet && r.URL.Path == "/v2/" {
			_, err := w.Write([]byte("ok"))
			require.NoError(t, err)

			return
		}

		calledServer++

		require.Equal(t, http.MethodGet, r.Method)
		require.Equal(t, "/v2/image/a/blobs/"+asd.String(), r.URL.Path)

		w.Header().Set("Content-Length", "3")
		w.Header().Set("Docker-Content-Digest", asd.String())

		_, err := w.Write([]byte("asd"))
		require.NoError(t, err)
	}))

	defer server.Close()

	// when
	cl := client.NewClient(dbc, nil, server.Client().Transport)

	in, err := client.ParseImageName(server.Listener.Addr().String() + "/image/a")
	require.NoError(t, err)

	err = cl.Blob.Get(context.Background(), in, asd)
	assert.NoError(t, err)

	isc, err := dbc.IsCached(context.Background(), asd)
	assert.NoError(t, err)
	assert.True(t, isc, "blob should be cached")
	assert.Equal(t, 1, calledServer)

	repo, err := dbc.ReadMeta(context.Background(), asd, client.MetaRepository)
	assert.NoError(t, err)
	assert.Equal(t, in.String(), string(repo))
}

func TestBlobGet_InvalidServerAnswer(t *testing.T) { //nolint:funlen // long test case.
	t.Parallel()

	asd := digest.FromString("asd")

	tests := []http.HandlerFunc{
		func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Length", "3")
			w.Header().Set("Docker-Content-Digest", asd.String())

			_, err := w.Write([]byte("asa")) // note differing bytes
			require.NoError(t, err)
		},
		func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Length", "5") // note differing length
			w.Header().Set("Docker-Content-Digest", asd.String())

			_, err := w.Write([]byte("asd"))
			require.NoError(t, err)
		},
		func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Length", "5")                                        // note differing length
			w.Header().Set("Docker-Content-Digest", digest.FromString("asdas").String()) // and other digest

			_, err := w.Write([]byte("asdas")) // for these bytes
			require.NoError(t, err)
		},
		func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Length", "3")
			w.Header().Set("Docker-Content-Digest", digest.FromString("asa").String()) // note differing digest

			_, err := w.Write([]byte("asa")) // and corresponding bytes
			require.NoError(t, err)
		},
		func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Length", "3")
			w.Header().Set("Docker-Content-Digest", digest.FromString("asa").String()) // note differing digest

			_, err := w.Write([]byte("asd"))
			require.NoError(t, err)
		},
	}

	for i := range tests { //nolint:paralleltest // Bug in linter?
		tst := tests[i]
		t.Run(fmt.Sprintf("test %d", i), func(t *testing.T) {
			t.Parallel()

			// given
			dbc := newTestCache(t)

			server := httptest.NewTLSServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				if r.Method == http.MethodGet && r.URL.Path == "/v2/" {
					_, err := w.Write([]byte("ok"))
					require.NoError(t, err)

					return
				}

				require.Equal(t, http.MethodGet, r.Method)
				require.Equal(t, "/v2/image/a/blobs/"+asd.String(), r.URL.Path)

				tst(w, r)
			}))

			defer server.Close()
			// when
			cl := client.NewClient(dbc, nil, server.Client().Transport)

			in, err := client.ParseImageName(server.Listener.Addr().String() + "/image/a")
			require.NoError(t, err)

			err = cl.Blob.Get(context.Background(), in, asd)
			assert.Error(t, err)

			isc, err := dbc.IsCached(context.Background(), asd)
			assert.NoError(t, err)
			assert.False(t, isc, "blob should not be cached")
		})
	}
}
