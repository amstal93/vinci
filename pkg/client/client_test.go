//+build ignore

// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package client_test

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/thriqon/vinci/pkg/client"
)

func TestClientAttemptPing(t *testing.T) { //nolint:funlen
	tlsServer := httptest.NewTLSServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, "HEAD", r.Method)
		require.Equal(t, "/v2/", r.URL.Path)
	}))
	defer tlsServer.Close()

	httpServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		require.Equal(t, "HEAD", r.Method)
		require.Equal(t, "/v2/", r.URL.Path)
	}))
	defer httpServer.Close()

	otherTLSServer := httptest.NewTLSServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "Not found", http.StatusNotFound)
	}))
	defer otherTLSServer.Close()

	otherHTTPServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "Not found", http.StatusNotFound)
	}))
	defer otherHTTPServer.Close()

	dbc := newTestCache(t)
	defer dbc.Close()

	tests := []struct {
		n  string
		c  *http.Client
		s  *httptest.Server
		ir []string
		f  assert.ErrorAssertionFunc
	}{
		{"v2 registry via tls", tlsServer.Client(), tlsServer, nil, assert.NoError},
		{"v2 registry via untrusted tls", nil, tlsServer, nil, assert.Error},
		{
			"v2 registry via untrusted, but accepted tls", nil, tlsServer,
			[]string{tlsServer.Listener.Addr().String()},
			assert.NoError,
		},
		{"v2 registry via http", httpServer.Client(), httpServer, nil, assert.Error},
		{
			"insecure v2 registry via http", httpServer.Client(), httpServer,
			[]string{httpServer.Listener.Addr().String()},
			assert.NoError,
		},

		{"other endpoint via tls", otherTLSServer.Client(), otherTLSServer, nil, assert.Error},
		{"other endpoint via untrusted tls", nil, otherTLSServer, nil, assert.Error},
		{
			"other endpoint via accepted, untrusted tls", nil, otherTLSServer,
			[]string{otherTLSServer.Listener.Addr().String()},
			assert.Error,
		},
		{"other endpoint via http", otherHTTPServer.Client(), otherHTTPServer, nil, assert.Error},
		{
			"other endpoint via accepted http", otherHTTPServer.Client(), otherHTTPServer,
			[]string{otherHTTPServer.Listener.Addr().String()},
			assert.Error,
		},
	}

	for _, tst := range tests {
		tst := tst
		t.Run(tst.n, func(t *testing.T) {
			cl := client.NewClient(dbc, tst.ir)
			if tst.c != nil {
				require.NotNil(t, tst.c.Transport)
				cl.AdoptTLSConfig(tst.c)
			}

			err := cl.TestPing(context.Background(), t, tst.s.Listener.Addr().String())
			tst.f(t, err)
		})
	}
}
