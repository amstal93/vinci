// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cache

import (
	"context"

	// The digest package does not use this algorithm directly.
	_ "crypto/sha256"
	"fmt"
	"hash"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"

	digest "github.com/opencontainers/go-digest"
)

const fileNameBlob = "blob"

// A dirBased cache is backed by a single directory on the local file system.
//
// The zero value is valid, probably using the current directory as base. Use
// DefaultCache() to get a cache backed by the preferred directory of the
// current user.
//
// While blobs and descriptors are written, they are stored in a temp directory. On Linux,
// the final directory will be moved to the correct name by one call to Rename().
type dirBased string

// DefaultCache returns a Cache configured to use the local file system in the
// user preferred location.
//
// On Linux, this is usually $HOME/.cache/vinci/v1/.
func DefaultCache() (Cache, error) {
	p, err := os.UserCacheDir()
	if err != nil {
		return nil, fmt.Errorf("get user cache dir for default cache: %w", err)
	}

	return dirBased(filepath.Join(p, "vinci", "v1")), nil
}

func InDirectory(dir string) Cache {
	return dirBased(dir)
}

func (db dirBased) nameFor(dgst digest.Digest, name ...string) string {
	s := dgst.Encoded()

	return filepath.Join(string(db), dgst.Algorithm().String()+":"+s[0:3], s[3:], filepath.Join(name...))
}

func (db dirBased) IsCached(ctx context.Context, dgst digest.Digest) (found bool, err error) {
	fname := db.nameFor(dgst)

	if _, err := os.Stat(fname); err != nil {
		if os.IsNotExist(err) {
			return false, nil
		}

		return false, fmt.Errorf("stat %s: %w", fname, err)
	}

	return true, nil
}

func (db dirBased) Open(ctx context.Context, dgst digest.Digest) (io.ReadCloser, int64, error) {
	blobfile, err := os.Open(db.nameFor(dgst, fileNameBlob))
	if err != nil {
		return nil, 0, fmt.Errorf("unable to open cached file: %w", err)
	}

	fi, err := blobfile.Stat()
	if err != nil {
		return nil, 0, fmt.Errorf("unable to stat file: %w", err)
	}

	return blobfile, fi.Size(), nil
}

func (db dirBased) ReadMeta(ctx context.Context, dgst digest.Digest, key string) ([]byte, error) {
	buf, err := ioutil.ReadFile(db.nameFor(dgst, "meta-"+key))
	if err != nil {
		return nil, fmt.Errorf("unable to read meta file for key %s: %w", key, err)
	}

	return buf, nil
}

type dirBasedCacheWrite struct {
	digester     digest.Digester
	h            hash.Hash
	bytesWritten int64

	tmpFile    *os.File
	tmpDir     string
	finalNameF func(digest.Digest, ...string) string

	isClosed  bool
	isSettled bool
}

var _ EntryWriter = (*dirBasedCacheWrite)(nil)

func (db dirBased) Create(ctx context.Context) (EntryWriter, error) {
	tmpbase := filepath.Join(string(db), "tmp")
	if err := os.MkdirAll(tmpbase, 0o700); err != nil {
		return nil, fmt.Errorf("unable to create temp base dir: %w", err)
	}

	tmpDir, err := ioutil.TempDir(tmpbase, "write-cache.*")
	if err != nil {
		return nil, fmt.Errorf("unable to create temp dir: %w", err)
	}

	tmpFile, err := os.Create(filepath.Join(tmpDir, fileNameBlob))
	if err != nil {
		_ = os.Remove(tmpDir)

		return nil, fmt.Errorf("unable to create blob file: %w", err)
	}

	digester := digest.SHA256.Digester()

	return &dirBasedCacheWrite{
		digester:     digester,
		h:            digester.Hash(),
		bytesWritten: 0,
		tmpDir:       tmpDir,
		tmpFile:      tmpFile,
		finalNameF:   db.nameFor,
	}, nil
}

func (cw *dirBasedCacheWrite) Write(p []byte) (n int, err error) {
	if cw.isClosed {
		return 0, fmt.Errorf("unable to write to file, already closed: %w", ErrWrongState)
	}

	_, _ = cw.h.Write(p)

	n, err = cw.tmpFile.Write(p)
	cw.bytesWritten += int64(n)

	return
}

func (cw *dirBasedCacheWrite) Close() error {
	if err := cw.tmpFile.Close(); err != nil {
		_ = os.RemoveAll(cw.tmpDir)

		return fmt.Errorf("unable to close cache file: %w", err)
	}

	cw.isClosed = true

	return nil
}

func (cw *dirBasedCacheWrite) Digest() digest.Digest {
	if !cw.isClosed {
		panic("unable to retrieve digest, file not closed")
	}

	return cw.digester.Digest()
}

func (cw *dirBasedCacheWrite) Size() int64 {
	if !cw.isClosed {
		panic("unable to retrieve size, file not closed")
	}

	return cw.bytesWritten
}

func (cw *dirBasedCacheWrite) Commit() error {
	if !cw.isClosed || cw.isSettled {
		return fmt.Errorf("unable to commit, not closed or already settled: %w", ErrWrongState)
	}

	targetName := cw.finalNameF(cw.digester.Digest())

	if err := os.MkdirAll(filepath.Dir(targetName), 0o755); err != nil {
		return fmt.Errorf("unable to create intermediate dirs for %v: %w", targetName, err)
	}

	if _, err := os.Stat(targetName); err == nil {
		// already exists, no need to attempt rename. Just silently discard and claim success.
		return cw.Discard()
	}

	if err := os.Rename(cw.tmpDir, targetName); err != nil {
		return fmt.Errorf("unable to move temp dir %s to final destination: %w", cw.tmpDir, err)
	}

	cw.isSettled = true

	return nil
}

func (cw *dirBasedCacheWrite) Discard() error {
	if !cw.isClosed || cw.isSettled {
		return fmt.Errorf("unable to discard, not closed or already settled: %w", ErrWrongState)
	}

	err := os.RemoveAll(cw.tmpDir)
	if err != nil {
		return fmt.Errorf("unable to remove temp file: %w", err)
	}

	cw.isSettled = true

	return nil
}

func (cw *dirBasedCacheWrite) SetMeta(ctx context.Context, key string, val []byte) error {
	err := ioutil.WriteFile(filepath.Join(cw.tmpDir, "meta-"+key), val, 0o600)
	if err != nil {
		return fmt.Errorf("set meta %s=%v: %w", key, val, err)
	}

	return nil
}
