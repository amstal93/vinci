// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cache_test

import (
	"context"
	"io"
	"io/ioutil"
	"os"
	"testing"

	digest "github.com/opencontainers/go-digest"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/thriqon/vinci/pkg/cache"
)

func TestCache(t *testing.T) {
	t.Parallel()

	factories := []struct {
		name string
		f    func(t *testing.T) cache.Cache
	}{
		{"dir based", func(t *testing.T) cache.Cache {
			t.Helper()

			tmpdir, err := ioutil.TempDir("", "test-cache.*")
			require.NoError(t, err)
			t.Cleanup(func() {
				require.NoError(t, os.RemoveAll(tmpdir))
			})

			return cache.InDirectory(tmpdir)
		}},
		{"memcache", func(t *testing.T) cache.Cache {
			t.Helper()

			return cache.NewMemCache()
		}},
	}

	for _, factory := range factories { //nolint:paralleltest // Bug in linter?
		factory := factory
		t.Run(factory.name, func(t *testing.T) {
			t.Parallel()

			t.Run("simple", func(t *testing.T) {
				t.Parallel()

				runSimpleTests(t, factory.f(t))
			})
			t.Run("duplicates", func(t *testing.T) {
				t.Parallel()

				runDuplicateWriteTests(t, factory.f(t))
			})
		})
	}
}

func runSimpleTests(t *testing.T, c cache.Cache) {
	t.Helper()

	isCached := func(d digest.Digest) bool {
		ic, err := c.IsCached(context.Background(), d)
		require.NoError(t, err)

		return ic
	}

	// when
	dig := digest.FromString("asd")
	assert.False(t, isCached(dig))

	wc, err := c.Create(context.Background())
	assert.NoError(t, err)

	n, err := io.WriteString(wc, "asd")
	assert.NoError(t, err)
	assert.Equal(t, 3, n)

	assert.NoError(t, wc.Close())

	assert.NoError(t, wc.SetMeta(context.Background(), "blubb", []byte("123")))

	assert.NoError(t, wc.Commit())

	//  then
	assert.Equal(t, digest.FromString("asd"), wc.Digest())
	assert.Equal(t, int64(len("asd")), wc.Size())

	// and
	assert.True(t, isCached(dig))

	rc, size, err := c.Open(context.Background(), digest.FromString("asd"))
	require.NoError(t, err)

	defer rc.Close()

	bytes, err := ioutil.ReadAll(rc)
	require.NoError(t, err)
	assert.Equal(t, "asd", string(bytes))
	assert.Equal(t, int64(3), size)

	buf, err := c.ReadMeta(context.Background(), digest.FromString("asd"), "blubb")
	assert.NoError(t, err)
	assert.Equal(t, "123", string(buf))

	_, err = c.ReadMeta(context.Background(), digest.FromString("asd"), "blubb2")
	assert.Error(t, err)
}

func runDuplicateWriteTests(t *testing.T, c cache.Cache) {
	t.Helper()

	dig := digest.FromString("asd2")

	writeAsd := func() error {
		wc, err := c.Create(context.Background())
		require.NoError(t, err)

		_, err = io.WriteString(wc, "asd2")
		require.NoError(t, err)

		require.NoError(t, wc.Close())

		return wc.Commit()
	}

	// when
	assert.NoError(t, writeAsd())

	// then
	assert.NoError(t, writeAsd())

	// and
	ic, err := c.IsCached(context.Background(), dig)
	assert.NoError(t, err)
	assert.True(t, ic)
}

func TestDirBased_Discard(t *testing.T) {
	t.Parallel()

	// given
	dbc := cache.InDirectory(t.TempDir())

	// when
	wc, err := dbc.Create(context.Background())
	require.NoError(t, err)

	_, err = io.WriteString(wc, "asd")
	require.NoError(t, err)

	require.NoError(t, wc.Close())

	require.NoError(t, wc.Discard())

	// then
	cached, err := dbc.IsCached(context.Background(), digest.FromString("asd"))
	assert.NoError(t, err)
	assert.False(t, cached)
}
