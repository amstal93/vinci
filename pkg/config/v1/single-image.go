// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package v1config

import (
	v1 "github.com/opencontainers/image-spec/specs-go/v1"
)

type SingleImageConfigFile struct {
	Version    string         `yaml:"spec-version"`
	ImageName  string         `yaml:"image"`
	Config     v1.ImageConfig `yaml:"config"`
	LayerSpecs []LayerSpec    `yaml:"layers"`
}

type LayerSpec struct {
	Combine   []LayerSpec    `yaml:"combine,omitempty"`
	File      *FileLayerSpec `yaml:"file,omitempty"`
	ImportOCI *string        `yaml:"importOCI,omitempty"`
	Import    *string        `yaml:"import,omitempty"`
}

type FileLayerSpec struct {
	Path     string
	Mode     int
	Contents string
	Source   string
}
