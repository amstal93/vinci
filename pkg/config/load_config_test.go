// Copyright 2021 Jonas Weber
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package config_test

import (
	"context"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/thriqon/vinci/pkg/config"
)

func TestLoad_FailInexistentVersion(t *testing.T) {
	t.Parallel()

	_, err := config.Load(context.Background(), strings.NewReader(`
spec-version: v0
image: test-image
spec: []
`))
	assert.Error(t, err)
	assert.NotContains(t, err.Error(), "yaml", "error message should not be from YAML decoder")
}

func TestLoad_MissingVersion(t *testing.T) {
	t.Parallel()

	_, err := config.Load(context.Background(), strings.NewReader(`
image: test-image
spec: []
`))
	assert.Error(t, err)
	assert.NotContains(t, err.Error(), "yaml", "error message should not be from YAML decoder")
}

func TestLoad(t *testing.T) {
	t.Parallel()

	cfg, err := config.Load(context.Background(), strings.NewReader(`
spec-version: v1
image: test-image
spec: []
`))
	assert.NoError(t, err)
	assert.Equal(t, "test-image", cfg.ImageName)
	assert.Empty(t, cfg.LayerSpecs)
}
